import Info from "./Info";
import ContextSample from "./ContextSample";

function App() {
  return (
    <div>
      <Info />
      <ContextSample />
    </div>
  );
}

export default App;

import React, { useContext, createContext } from "react";

const ThemeContext = createContext("black");
const ContextSample = () => {
  const theme = useContext(ThemeContext);
  const style = {
    width: "24px",
    height: "24px",
    backgroundColor: theme,
  };

  return <div style={style} />;
};

export default ContextSample;

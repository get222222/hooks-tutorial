import React, { useReducer } from "react";

function reducer(state, action) {
  switch (action.type) {
    case "increase":
      return { value: state.value + 1 };
    case "decrease":
      return { value: state.value - 1 };
    default:
      return state;
  }
}
//1
//2
//3
//4
const Counter = () => {
  const [state, dispatch] = useReducer(reducer, { value: 0 });

  return (
    <div>
      <p>
        현재 카운트 값음 <b>{state.value}</b> 입니다.
      </p>
      <button onClick={() => dispatch({ type: "increase" })}>+1</button>
      <button onClick={() => dispatch({ type: "decrease" })}>-1</button>
    </div>
  );
};

export default Counter;
